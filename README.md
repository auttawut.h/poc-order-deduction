# POC deduct create order credit
This is POC of comparison deducting order credit between locking DB row VS using serialized transaction.
- DB Lock is simple but risky for deadlock and poor performance.
- Serializable transaction is a modern way to cease race condition but retrying transactions is complicated.

# HOW to run.
1. Create an image from the Dockerfile and run with
```
$ docker run --name some-postgres -e POSTGRES_PASSWORD=password -p 5432:5432
```
2. Run 
```
$ go test ./.. 
```

# TODO
- Add benchmarks.