module order_credit

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.4
)
