package orders

import (
	"context"
	"database/sql"
	"gorm.io/gorm"
	"strings"
	"time"
)

const userID = "001"

func createCreditTXBySerializedTX(ctx context.Context, db *gorm.DB, name string) error {
	isolationLevel := sql.TxOptions{
		Isolation: sql.LevelSerializable,
	}

	retryCount := 0
	var err error

	for retryCount < 50 {
		err = db.Transaction(func(tx *gorm.DB) error {
			newOrder := order{Name: name}

			// Get remaining active credit.
			var currentCredit int
			if err := tx.Raw(`
				SELECT sum(credit)
				FROM credit_tx
				WHERE user_id = ? AND CURRENT_DATE >= start_date AND CURRENT_DATE <= end_date
			`, userID).Scan(&currentCredit).Error; err != nil {
				return err
			}

			// Decide if it is deductible.
			if currentCredit <= 0 {
				return noCreditAvailableErr
			}

			// Insert credit deduction and create order.
			creditTX := creditTX{
				Credit:    -1,
				UserID:    "001",
				StartDate: time.Now(),
				EndDate:   time.Now(),
				Source:    "order",
			}
			if err := tx.Create(&creditTX).Error; err != nil {
				return err
			}

			result := tx.Create(&newOrder)
			if result.Error != nil {
				return result.Error
			}

			creditOrder := creditOrder{
				CreditTXSID: creditTX.ID,
				OrdersID:    newOrder.ID,
			}

			if err := tx.Create(creditOrder).Error; err != nil {
				return err
			}

			return nil
		}, &isolationLevel)

		if err != nil && strings.Contains(err.Error(), "SQLSTATE 40001") {
			// If serialize tx error , retry.
			retryCount++
			// Wait time should be exponential backoff.
			time.Sleep(time.Millisecond * 200)
		} else {
			break
		}
	}

	return err
}
