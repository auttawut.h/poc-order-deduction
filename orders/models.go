package orders

import (
	"errors"
	"time"
)

var noCreditAvailableErr = errors.New("no more credit available")

type order struct {
	ID   int
	Name string
}

type creditStore struct {
	Credit int
}

type creditTX struct {
	ID        int
	Credit    int
	UserID    string
	StartDate time.Time
	EndDate   time.Time
	Source    string
}

func (creditTX) TableName() string {
	return "credit_tx"
}

type creditOrder struct {
	CreditTXSID int
	OrdersID    int
}
