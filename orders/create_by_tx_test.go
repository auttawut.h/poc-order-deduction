package orders

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"strconv"
	"sync"
	"testing"
)

func TestCreateBySerializeTX(t *testing.T) {
	db, err := gorm.Open(postgres.Open(dsn))
	assert.NoError(t, err)

	db.Exec(`DELETE FROM orders`)
	db.Exec(`DELETE FROM credit_stores`)

	creditAvail := 10
	numOfOrderTX := 30

	iCredit := creditStore{Credit: creditAvail}
	db.Create(iCredit)

	var wg sync.WaitGroup

	for i := 0; i < numOfOrderTX; i++ {
		wg.Add(1)

		go func(suffix string) {
			_ = createOrderBySerializedTX(context.Background(), db, "order_")

			wg.Done()
		}(strconv.Itoa(i))
	}

	wg.Wait()

	var credit creditStore
	db.Find(&credit)

	// All credit must be deducted and not a negative value.
	assert.Equal(t, 0, credit.Credit)

	var orderCount int64
	db.Model(order{}).Count(&orderCount)

	// Number of order must be an available credit.
	assert.Equal(t, int64(creditAvail), orderCount)
}
