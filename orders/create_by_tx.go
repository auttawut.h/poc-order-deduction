package orders

import (
	"context"
	"database/sql"
	"gorm.io/gorm"
	"strings"
	"time"
)

func createOrderBySerializedTX(ctx context.Context, db *gorm.DB, name string) error {
	isolationLevel := sql.TxOptions{
		Isolation: sql.LevelSerializable,
	}

	retryCount := 0
	var err error

	for retryCount < 5 {
		err = db.Transaction(func(tx *gorm.DB) error {
			newOrder := order{Name: name}

			var currentCredit creditStore
			tx.First(&currentCredit)

			if currentCredit.Credit <= 0 {
				return noCreditAvailableErr
			}

			result := tx.Create(&newOrder)
			if result.Error != nil {
				return result.Error
			}

			deductResult := tx.Exec(`
			UPDATE credit_stores
			SET credit = credit - 1
		`)
			return deductResult.Error
		}, &isolationLevel)

		if err != nil && strings.Contains(err.Error(), "SQLSTATE 40001") {
			// If serialize tx error , retry.
			retryCount++
			// Wait time should be exponential backoff.
			time.Sleep(time.Millisecond * 200)
		} else {
			break
		}
	}

	return err
}
