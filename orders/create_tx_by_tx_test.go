package orders

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"golang.org/x/sync/semaphore"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"strconv"
	"testing"
)

func TestCreateCreditTX_BYTX(t *testing.T) {
	db, err := gorm.Open(postgres.Open(dsn))
	assert.NoError(t, err)

	var maxConcurrent int64 = 200

	sem := semaphore.NewWeighted(maxConcurrent)

	for i := 0; i < 201; i++ {
		if err := sem.Acquire(context.Background(), 1); err != nil {
			log.Println("cannot acquire semaphore")
			return
		}

		go func(name int) {
			defer sem.Release(1)
			if err := createCreditTXBySerializedTX(context.Background(), db, "order_"+strconv.Itoa(name)); err != nil {
				log.Println("error create new order: ", err.Error())
			}
		}(i)
	}

	if err := sem.Acquire(context.Background(), maxConcurrent); err != nil {
		fmt.Println("error acquire all semaphore, ", err.Error())
	}

}
