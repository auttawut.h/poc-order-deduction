package orders

import (
	"context"
	"gorm.io/gorm"
)

func createOrderByLocking(ctx context.Context, db *gorm.DB, name string) error {
	return db.Transaction(func(tx *gorm.DB) error {
		newOrder := order{Name: name}

		var currentCredit creditStore
		tx.Raw(
			`
				 SELECT credit
				 FROM credit_stores
				 FOR NO KEY UPDATE
		`).Scan(&currentCredit)

		if currentCredit.Credit <= 0 {
			return noCreditAvailableErr
		}

		result := tx.Create(&newOrder)
		if result.Error != nil {
			return result.Error
		}

		deductResult := tx.Exec(`
			UPDATE credit_stores
			SET credit = credit - 1
		`)
		return deductResult.Error
	})
}
