FROM postgres:9.6.23-alpine

WORKDIR /
COPY ./sql_scripts/* /docker-entrypoint-initdb.d/