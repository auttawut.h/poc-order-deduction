CREATE TABLE credit_tx
(
    id         SERIAL PRIMARY KEY,
    credit     INTEGER,
    user_id    TEXT,
    start_date DATE,
    end_date   DATE,
    source     TEXT
);

INSERT INTO credit_tx
VALUES (DEFAULT, 100, '001', CURRENT_DATE, CURRENT_DATE + INTERVAL '30', 'subscription'),
       (DEFAULT, 100, '001', CURRENT_DATE - INTERVAL '30', CURRENT_DATE - INTERVAL '1', 'subscription'),
       (DEFAULT, 100, '001', CURRENT_DATE + INTERVAL '31', CURRENT_DATE - INTERVAL '61', 'subscription');
